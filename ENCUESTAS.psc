//https://github.com/c5060762/ENCUESTAS
//author: c5060762@gmail.com
Funcion constante<-VACIO()
	constante	<-""
FinFuncion
Funcion constante<-ESPACIO()
	constante	<-" "
FinFuncion
Funcion constante<-ALINEAR_IZQUIERDA()
	constante<-Verdadero
FinFuncion
Funcion constante<-FIN_LINEA()
	constante	<-"*"
FinFuncion
Funcion constante<-INTERROGACION()
	constante	<-"?"
FinFuncion
Funcion constante<-N_PRODUCTOS()
	constante	<-07
FinFuncion
Funcion constante<-N_TIENDAS()
	constante	<-05
FinFuncion
Funcion constante<-N_VENDEDORES()
	constante	<-20
FinFuncion
Funcion constante<-N_NIVEL_SATISFACCION()
	constante	<-04
FinFuncion
Funcion constante<-CARACTER_FIN_ARCHIVO()
	constante	<-"0"
FinFuncion
Funcion constante<-N_MIN_ENCUESTAS()
	constante	<-5
FinFuncion
Funcion constante<-N_MAX_ENCUESTAS()
	constante	<-15
FinFuncion
Funcion constante<-OFFSET()
	constante	<-1
FinFuncion
Funcion constante<-I_PRODUCTO()
	constante	<-cero
FinFuncion
Funcion constante<-I_TIENDA()
	constante	<-I_PRODUCTO+1
FinFuncion
Funcion constante<-I_VENDEDOR()
	constante	<-I_TIENDA+1
FinFuncion
Funcion constante<-I_NIVEL()
	constante	<-I_VENDEDOR+1
FinFuncion
Funcion constante<-N_CAMPOS_ENCUESTA()
	constante	<-I_NIVEL+1
FinFuncion
Funcion constante<-INDICE_INVALIDO()
	constante	<--1
FinFuncion
Funcion constante<-ANCHO_COLUMNA()
	constante	<-6
FinFuncion
Funcion constante<-LIMPIAR_PANTALLA()
	constante	<-falso
FinFuncion
Funcion constante<-ETIQUETA_TIENDA()
	constante	<-"T"
FinFuncion
Funcion constante<-ETIQUETA_PRODUCTO()
	constante	<-"P"
FinFuncion
Funcion constante<-ETIQUETA_VENDEDOR()
	constante	<-"V"
FinFuncion
Funcion constante<-ETIQUETA_NIVEL()
	constante	<-"N"
FinFuncion
Funcion constante<-TITULO_MATRIZ_TIENDAS_NIVELES()
	constante	<-"Tiendas vs. Niveles de Satisfacción"
FinFuncion
Funcion constante<-TITULO_MATRIZ_TIENDAS_PRODUCTOS()
	constante	<-"Tiendas vs. Productos"
FinFuncion
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
Funcion cRellenado<-Rellenar(caracterRellenar, anchoColumna, caracterRelleno, alineacion)
	cRellenado			<-VACIO
	anchoCaracter		<-Longitud(caracterRellenar)
	nEspaciosRelleno	<-anchoColumna-anchoCaracter
	Si nEspaciosRelleno es positivo Entonces
		relleno			<-VACIO
		Para e desde 1 hasta nEspaciosRelleno Hacer
			relleno		<-Concatenar(relleno,caracterRelleno)
		FinPara
		Si alineacion es ALINEAR_IZQUIERDA Entonces
			cRellenado	<-Concatenar(caracterRellenar,relleno)
		SiNo
			cRellenado	<-Concatenar(relleno,caracterRellenar)
		FinSi
	SiNo
		cRellenado		<-caracterRellenar
	FinSi
FinFuncion
Funcion cAlineado<-Alinear(caracterAlinear, anchoColumna, alineacion)
	cAlineado			<-Rellenar(caracterAlinear,anchoColumna,ESPACIO,alineacion)
FinFuncion
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
SubProceso PAUSA()
	mensajePausa	<-"Presione enter para continuar..."
	Escribir mensajePausa
	Esperar tecla
FinSubProceso
SubProceso SALTAR_LINEA()
	Escribir VACIO
FinSubProceso
Subproceso SALTAR_LINEAS(nLineas)
	Mientras nLineas es mayor que cero Hacer
		SALTAR_LINEA
		nLineas		<-nLineas-1
	FinMientras
FinSubProceso
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
Funcion encuesta<-GenerarEncuesta()
	producto	<-ConvertirATexto(azar(N_PRODUCTOS)			+OFFSET)
	tienda		<-ConvertirATexto(azar(N_TIENDAS)			+OFFSET)
	vendedor	<-ConvertirATexto(azar(N_VENDEDORES)		+OFFSET)
	nivel		<-ConvertirATexto(azar(N_NIVEL_SATISFACCION)+OFFSET)
	producto	<-Concatenar(producto,ESPACIO)
	tienda		<-Concatenar(tienda,ESPACIO)
	vendedor	<-Concatenar(vendedor,ESPACIO)
	nivel		<-Concatenar(nivel,ESPACIO)
	encuesta	<-Concatenar(producto,tienda)
	encuesta	<-Concatenar(encuesta,vendedor)
	encuesta	<-Concatenar(encuesta,nivel)
	encuesta	<-Concatenar(encuesta,FIN_LINEA)
FinFuncion
Funcion archivoEncuestas<-GenerarArchivoEncuestas(nEncuestas)
	archivoEncuestas	<-VACIO
	finArchivo			<-Concatenar(CARACTER_FIN_ARCHIVO,ESPACIO)
	Para e desde 0 hasta nEncuestas-1 Hacer
		archivoEncuestas<-Concatenar(archivoEncuestas,GenerarEncuesta())
	FinPara
	archivoEncuestas	<-Concatenar(archivoEncuestas,finArchivo)
FinFuncion
Funcion MostrarArchivoEncuestas(archivoEncuestas)
	e							<-0
	noHayMasLetras				<-falso
	letraAnterior				<-VACIO
	SALTAR_LINEA
	titulo						<-"Archivo de Encuestas"
	Escribir titulo
	SALTAR_LINEA
	Repetir
		letra<-Subcadena(archivoEncuestas,e,e)
		Si letra es FIN_LINEA Entonces
			SALTAR_LINEA
		SiNo
			Escribir sin saltar letra
			Si letra es CARACTER_FIN_ARCHIVO y letraAnterior es FIN_LINEA Entonces
				noHayMasLetras	<-Verdadero
			FinSi
		FinSi
		letraAnterior			<-letra
		e						<-e+1
	Hasta Que noHayMasLetras
	SALTAR_LINEA
FinFuncion
Funcion nValorEntero<-LeerEntero(bordeSuperior, mensaje)
	Repetir
		Repetir
			Escribir sin saltar mensaje,INTERROGACION,ESPACIO
			Leer nValorEntero
		Hasta Que nValorEntero es menor que bordeSuperior
	Hasta Que nValorEntero no Es Negativo
FinFuncion
Funcion nEncuestas<-GenerarNumeroAzarEncuestas()
	nEncuestas					<-0
	titulo						<-"nEncuestas"
	Hacer
		nEncuestas<-azar(N_MAX_ENCUESTAS)
	Hasta Que nEncuestas es mayor o igual que N_MIN_ENCUESTAS
	Escribir titulo,ESPACIO,nEncuestas
FinFuncion
SubProceso ProbarGenerarArchivoEncuestas()
	mensajeInicio				<-"Inicio - ProbarGenerarArchivoEncuestas()"
	mensajeFin					<-"Fin - ProbarGenerarArchivoEncuestas()"
	Escribir mensajeInicio
	nEncuestas	<-GenerarNumeroAzarEncuestas()
	Si nEncuestas es mayor que cero Entonces
		archivoEncuestas<-GenerarArchivoEncuestas(nEncuestas)
		MostrarArchivoEncuestas(archivoEncuestas)
	FinSi
	Escribir mensajeFin
FinSubProceso
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
Subproceso ExtraerInformacionEncuesta(lineaArchivoEncuestas, camposEncuesta Por Referencia)
	i0								<-0
	iF								<-Longitud(lineaArchivoEncuestas)
	iCampo							<-0 //I_PRODUCTO o I_TIENDA o I_VENDEDOR o I_NIVEL
	i								<-i0
	Hacer
		letra						<-Subcadena(lineaArchivoEncuestas,i,i)
		Si letra es ESPACIO //no procesa finLinea ni finArchivo
			textoCantidad			<-Subcadena(lineaArchivoEncuestas,i0,i-1)
			camposEncuesta[iCampo]	<-ConvertirANumero(textoCantidad)
			iCampo					<-iCampo+1
			i0						<-i+1
		FinSi
		i							<-i+1
	hasta que iCampo es N_CAMPOS_ENCUESTA
FinSubProceso
Funcion lineaArchivoEncuestas<-ExtraerLineaArchivoEncuestas(archivoEncuestas,i0 Por Referencia)
	i								<-i0
	lineaArchivoEncuestas			<-VACIO
	letra							<-VACIO
	letraAnterior					<-VACIO
	noHayMasLetras					<-falso
	Hacer
		letra						<-Subcadena(archivoEncuestas, i,i)
		Si letra no es FIN_LINEA Entonces
			lineaArchivoEncuestas	<-Concatenar(lineaArchivoEncuestas,letra)
		FinSi		
		i							<-i+1
	Hasta Que letra es FIN_LINEA
	i0								<-i
	letra							<-Subcadena(archivoEncuestas, i,i)
	Si letra es CARACTER_FIN_ARCHIVO Entonces
		i0							<-INDICE_INVALIDO
	FinSi
FinFuncion
Subproceso InicializarMatrizConCeros(matriz por referencia)
	Para cada elemento de matriz Hacer
		elemento					<-cero
	FinPara
FinSubProceso
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
SubProceso DibujarLinea(anchoLinea,caracterLinea)
	Escribir sin saltar Rellenar(VACIO,anchoLinea,caracterLinea,falso)
FinSubProceso
SubProceso MostrarMatriz(matriz,nFilas,nColumnas,etiquetaFilas,etiquetaColumnas,tituloMatriz)
	Si LIMPIAR_PANTALLA es Verdadero
		Limpiar Pantalla
	FinSi
	totalFilas					<-0
	totalColumnas				<-0
	alineacion					<-no ALINEAR_IZQUIERDA
	lVertical					<-"|"
	lHorizontal					<-"_"
	Dimension sumaColumnas[nColumnas]
	SALTAR_LINEA
	Escribir 					tituloMatriz
	Escribir sin saltar Alinear(VACIO, ANCHO_COLUMNA,alineacion)
	Para j desde 0 hasta nColumnas-1 Hacer
		etiqueta				<-Concatenar(etiquetaColumnas,ConvertirATexto(j+OFFSET))
		Escribir sin saltar 	Alinear(etiqueta, ANCHO_COLUMNA,alineacion)
		Para i desde 0 hasta nFilas-1 Hacer
			sumaColumnas[j]		<-matriz[i,j]+sumaColumnas[j]
		FinPara
	FinPara
	SALTAR_LINEA
	Para i desde 0 hasta nFilas-1 Hacer
		etiqueta				<-Concatenar(etiquetaFilas,ConvertirATexto(i+OFFSET))
		sumaFila				<-0
		Escribir sin saltar Alinear(etiqueta,ANCHO_COLUMNA,no alineacion)
		Para j desde 0 hasta nColumnas-1 Hacer
			numeroElemento		<-matriz[i,j]
			Si numeroElemento no es cero Entonces
				sumaFila		<-numeroElemento+sumaFila
				elemento		<-ConvertirATexto(numeroElemento)
			SiNo
				elemento		<-VACIO
			FinSi
			Escribir sin saltar Alinear(elemento,ANCHO_COLUMNA,alineacion)
		FinPara
		totalFilas				<-totalFilas+sumaFila
		textoSumaFila			<-ConvertirATexto(sumaFila)
		Escribir sin saltar 	lVertical Alinear(textoSumaFila, ANCHO_COLUMNA, alineacion)
		SALTAR_LINEA
	FinPara
	Escribir sin saltar 		Alinear(VACIO, ANCHO_COLUMNA,alineacion)
	DibujarLinea(ANCHO_COLUMNA*nColumnas,lHorizontal)
	Escribir 					lVertical
	Para j desde 0 hasta nColumnas-1 Hacer
		Si j es 0 Entonces
			Escribir sin saltar Alinear(VACIO, ANCHO_COLUMNA,alineacion)
		FinSi
		totalColumnas		<-sumaColumnas[j]+totalColumnas
		Escribir sin saltar Alinear(ConvertirATexto(sumaColumnas[j]),ANCHO_COLUMNA,alineacion)
	FinPara
	totales					<-ConvertirATexto(totalColumnas)
	totales					<-Concatenar(totales,lVertical)
	totales					<-Concatenar(totales,ConvertirATexto(totalFilas))
	Escribir 				lVertical Alinear(totales,ANCHO_COLUMNA,alineacion)
	SALTAR_LINEA
FinSubProceso
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
SubProceso ArmarMatrizTiendasNiveles(matrizTiendasNiveles Por Referencia, archivoEncuestas)
	i													<-0
	Dimension 											camposEncuesta[N_CAMPOS_ENCUESTA]
	Hacer
		ExtraerInformacionEncuesta(ExtraerLineaArchivoEncuestas(archivoEncuestas,i),camposEncuesta)
		Si camposEncuesta[I_PRODUCTO] no es cero Entonces
			iTienda										<-camposEncuesta[I_TIENDA]-OFFSET
			iNivel										<-camposEncuesta[I_NIVEL]-OFFSET
			matrizTiendasNiveles[iTienda,iNivel]		<-matrizTiendasNiveles[iTienda,iNivel]+1
		FinSi
	Hasta Que i eS INDICE_INVALIDO
FinSubProceso
SubProceso ArmarMatrizTiendasProductos(matrizTiendasProductos Por Referencia, archivoEncuestas)
	i													<-0
	Dimension 											camposEncuesta[N_CAMPOS_ENCUESTA]
	Hacer
		linea<-ExtraerLineaArchivoEncuestas(archivoEncuestas,i)
		ExtraerInformacionEncuesta(linea, camposEncuesta)
		Si camposEncuesta[I_PRODUCTO] no es cero Entonces
			iTienda										<-camposEncuesta[I_TIENDA]-OFFSET
			iProducto									<-camposEncuesta[I_PRODUCTO]-OFFSET
			matrizTiendasProductos[iTienda,iProducto]	<-matrizTiendasProductos[iTienda,iProducto]+1
		FinSi
	Hasta Que i eS INDICE_INVALIDO
FinSubProceso
Subproceso ArmarMostrarVectorProductosVendidosPorVendedor(vectorProductosVendidos Por Referencia, archivoEncuestas)
	titulo												<-"Venta de productos por vendedor"
	anchoColumna										<-5
	alineacion											<-no ALINEAR_IZQUIERDA
	i													<-0
	etiquetaVendedor									<-"V"
	etiquetaProductos									<-"#Prod"
	sumaProductos										<-0
	etiquetaTotal										<-"TOTAL"
	caracterGuion										<-"_"
	caracterVertical									<-"|"
	iMejorVendedor										<-INDICE_INVALIDO
	nMejorVentaVendedor											<-0
	Dimension 											camposEncuesta[N_CAMPOS_ENCUESTA]
	Para cada elemento de vectorProductosVendidos Hacer
		elemento										<-cero
	FinPara
	Hacer
		linea											<-ExtraerLineaArchivoEncuestas(archivoEncuestas,i)
		ExtraerInformacionEncuesta(linea,camposEncuesta)
		Si camposEncuesta[I_PRODUCTO] no es cero Entonces
			iVendedor									<-camposEncuesta[I_VENDEDOR]-OFFSET
			vectorProductosVendidos[iVendedor]			<-vectorProductosVendidos[iVendedor]+1
		FinSi
	Hasta Que i eS INDICE_INVALIDO
	Escribir 											titulo
	Escribir sin saltar 								Alinear(VACIO,anchoColumna,alineacion)	
	Para v desde 0 hasta N_VENDEDORES-1 Hacer
		textoNumero										<-ConvertirATexto(v+OFFSET)
		textoVendedor									<-Concatenar(etiquetaVendedor,textoNumero)
		Escribir sin saltar 							Alinear(textoVendedor,anchoColumna,alineacion)
	FinPara
	SALTAR_LINEA
	DibujarLinea(anchoColumna*(N_VENDEDORES+1),caracterGuion)
	Escribir											caracterVertical Alinear(etiquetaTotal,anchoColumna*2,alineacion)
	Escribir sin saltar 								Alinear(etiquetaProductos,anchoColumna,alineacion)
	Para v desde 0 hasta N_VENDEDORES-1 hacer
		nProductos										<-vectorProductosVendidos[v]
		si nProductos es mayor que nMejorVentaVendedor Entonces
			nMejorVentaVendedor							<-nProductos
			iMejorVendedor								<-v
		FinSi
		sumaProductos									<-sumaProductos+nProductos
		txtProductosVendidos							<-ConvertirATexto(nProductos)
		Escribir sin saltar 							Alinear(txtProductosVendidos,anchoColumna,alineacion)
	FinPara
	Escribir sin saltar									caracterVertical Alinear(ConvertirATexto(sumaProductos),anchoColumna*2,alineacion)
	SALTAR_LINEAS(2)
	Escribir 											"Mejor(es) vendedor(es)"
	Escribir sin saltar 								"El vendedor No." iMejorVendedor+OFFSET ESPACIO "vendió" ESPACIO
	Escribir 											nMejorVentaVendedor ESPACIO "productos."
	Para v desde iMejorVendedor+1 hasta N_VENDEDORES-1 con paso 1 Hacer
		Si vectorProductosVendidos[v] es igual a nMejorVentaVendedor Entonces
			Escribir sin saltar							"El vendedor No." v+OFFSET ESPACIO "también vendió" ESPACIO
			Escribir 									nMejorVentaVendedor ESPACIO "productos."
		FinSi
	FinPara
FinSubProceso
SubProceso MostrarInformeFinal(mTiendasNiveles, mTiendasProductos)
	nMejorVentaVendedor							<-0
	nPeorNivel									<-0
	nTienda										<-0
	nProducto									<-0
	iTienda										<-0
	iProducto									<-0
	iVentas										<-iTienda+1
	iNivel										<-iTienda+1	
	nColumnas									<-iVentas+1	
	anchoColumna								<-nColumnas*6
	alineacion									<-no ALINEAR_IZQUIERDA
	iNivelMuyInsatisfecho						<-N_NIVEL_SATISFACCION-1
	nProductosVendidos							<-0
	nProductoMejorVenta							<-0
	Dimension 									tiendasMejorVenta[N_TIENDAS,nColumnas]
	Dimension 									tiendasPeorNivel[N_TIENDAS,nColumnas]
	Dimension 									ventasProducto[N_PRODUCTOS,nColumnas]
	Para i desde 0 hasta N_TIENDAS-1 Hacer
		ventaTienda								<-0
		para j desde 0 hasta N_PRODUCTOS-1 Hacer
			ventaTienda							<-mTiendasProductos[i,j]+ventaTienda
		FinPara
		Si ventaTienda es mayor o igual que nMejorVentaVendedor Entonces
			nMejorVentaVendedor					<-ventaTienda
			tiendasMejorVenta[nTienda,iTienda]	<-i+OFFSET
			tiendasMejorVenta[nTienda,iVentas]	<-ventaTienda
			nTienda								<-nTienda+1
		FinSi
	FinPara
	SALTAR_LINEA
	Escribir sin saltar							"La o las tiendas con mejores ventas vendieron" 
	Escribir 									ESPACIO nMejorVentaVendedor ESPACIO "productos."
	Escribir sin saltar 						Alinear("No.Tienda",anchoColumna,alineacion)
	Escribir 									Alinear("#Prod",anchoColumna,alineacion)
	Para i desde nTienda-1 hasta 0 con paso -1 Hacer
		numeroTienda							<-tiendasMejorVenta[i,iTienda]
		numeroVenta								<-tiendasMejorVenta[i,iVentas]
		Si numeroVenta es nMejorVentaVendedor Entonces			
			textoTienda							<-ConvertirATexto(numeroTienda)
			textoVenta							<-ConvertirATexto(numeroVenta)
			textoTienda							<-Alinear(textoTienda,anchoColumna,alineacion)
			textoVenta							<-Alinear(textoVenta,anchoColumna,alineacion)
			Escribir 							textoTienda textoVenta			
		FinSi
	FinPara
	SALTAR_LINEA
	nTienda										<-0
	Para i desde 0 hasta N_TIENDAS-1 Hacer
		nivelTienda								<-mTiendasNiveles[i,iNivelMuyInsatisfecho]
		Si nivelTienda es mayor o igual que nPeorNivel Entonces
			nPeorNivel							<-nivelTienda
			tiendasPeorNivel[nTienda,iTienda]	<-i+OFFSET
			tiendasPeorNivel[nTienda,iNivel]	<-nivelTienda
			nTienda								<-nTienda+1
		FinSi
	FinPara
	Escribir sin saltar							"La o las tiendas con peor nivel de satisfacción tuvieron"
	Escribir 									ESPACIO nPeorNivel ESPACIO "encuestas con clientes muy insatisfechos"
	Escribir sin saltar 						Alinear("No.Tienda",anchoColumna,alineacion)
	Escribir 									Alinear("#Encuestas",anchoColumna,alineacion)
	Para i desde N_TIENDAS-1 hasta 0 con paso -1 Hacer
		numeroTienda							<-tiendasPeorNivel[i,iTienda]
		numeroNivel								<-tiendasPeorNivel[i,iNivel]
		Si tiendasPeorNivel[i,iNivel] es nPeorNivel Entonces
			textoTienda							<-ConvertirATexto(numeroTienda)
			textoNivel							<-ConvertirATexto(numeroNivel)
			textoTienda							<-Alinear(textoTienda,anchoColumna,alineacion)
			textoNivel							<-Alinear(textoNivel,anchoColumna,alineacion)
			Escribir 							textoTienda textoNivel
		FinSi
	FinPara
	Para j desde 0 hasta N_PRODUCTOS-1 Hacer
		nProductosVendidos						<-0
		Para i desde 0 hasta N_TIENDAS-1 Hacer
			nProductosVendidos					<-nProductosVendidos+mTiendasProductos[i,j]
		FinPara
		Si nProductosVendidos es mayor o igual que nProductoMejorVenta Entonces
			nProductoMejorVenta					<-nProductosVendidos
			ventasProducto[nProducto,iProducto]	<-j+OFFSET
			ventasProducto[nProducto,iVentas]	<-nProductosVendidos
			nProducto							<-nProducto+1
		FinSi
	FinPara
	nProducto									<-nProducto-1
	SALTAR_LINEA
	Escribir sin saltar							"El (los) producto(s) más vendido(s) es (son) el producto No." 
	Escribir sin saltar							ventasProducto[nProducto,iProducto] ESPACIO "(" nProductoMejorVenta ")"
	Escribir 									ESPACIO "unidades."
	Para p desde nProducto-1 hasta 0 con paso -1 Hacer
		Si ventasProducto[p,iventas] es nProductoMejorVenta Entonces
			Escribir sin saltar "El producto" ESPACIO ventasProducto[p,iProducto] ESPACIO 
			Escribir "también vendió" ESPACIO nProductoMejorVenta ESPACIO "unidades."
		FinSi
	FinPara
	SALTAR_LINEA
FinSubProceso
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
Proceso ENCUESTAS
	archivoEncuestas											<-GenerarArchivoEncuestas(GenerarNumeroAzarEncuestas())
	Dimension 													tiendasNiveles[N_TIENDAS,N_NIVEL_SATISFACCION]
	Dimension 													tiendasProductos[N_TIENDAS,N_PRODUCTOS]
	Dimension													productosVendidosPorVendedor[N_VENDEDORES]
	InicializarMatrizConCeros(tiendasNiveles)
	MostrarArchivoEncuestas(archivoEncuestas)
	PAUSA
	ArmarMatrizTiendasNiveles(tiendasNiveles,archivoEncuestas)
	ArmarMatrizTiendasProductos(tiendasProductos,archivoEncuestas)
	MostrarMatriz(tiendasNiveles,N_TIENDAS,N_NIVEL_SATISFACCION,ETIQUETA_TIENDA,ETIQUETA_NIVEL,TITULO_MATRIZ_TIENDAS_NIVELES)
	MostrarMatriz(tiendasProductos,N_TIENDAS,N_PRODUCTOS,ETIQUETA_TIENDA,ETIQUETA_PRODUCTO,TITULO_MATRIZ_TIENDAS_PRODUCTOS)
	MostrarInformeFinal(tiendasNiveles,tiendasProductos)
	ArmarMostrarVectorProductosVendidosPorVendedor(productosVendidosPorVendedor,archivoEncuestas)
FinProceso
